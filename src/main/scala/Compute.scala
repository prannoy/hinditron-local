/**
 * Created by prannoysircar on 3/26/16.
 */

class Compute(constantFields: String, elementsValue: Array[Double]) {
  def compute() {

    val C   =  elementsValue(1)
    val SI  =  elementsValue(2)
    val MN  =  elementsValue(3)
    val P   =  elementsValue(4)
    val S   =  elementsValue(5)
    val CR  =  elementsValue(6)
    val MO  =  elementsValue(7)
    val NI  =  elementsValue(8)
    val CU  =  elementsValue(9)
    val SN  =  elementsValue(10)
    val AI  =  elementsValue(11)
    val V   =  elementsValue(12)
    val B   =  elementsValue(13)
    val TI  =  elementsValue(14)
    val NB  =  elementsValue(15)
    val W   =  elementsValue(16)
    val CO  =  elementsValue(17)
    val SB  =  elementsValue(18)
    val AS  =  elementsValue(19)
    val TE  =  elementsValue(20)
    val CA  =  elementsValue(21)
    val N2  =  elementsValue(22)
    val H2  =  elementsValue(23)
    val BI  =  elementsValue(24)
    val O2  =  elementsValue(25)
    val PB  =  elementsValue(26)
    val ZN  =  elementsValue(27)
    val ZR  =  elementsValue(28)
    val SE  =  elementsValue(29)
    val CD  =  elementsValue(30)
    val AL  =  elementsValue(31)
    val ALS  =  elementsValue(32)
    val ALO  =  elementsValue(33)
    val BS  =  elementsValue(34)
    val BO  =  elementsValue(35)
    val N  =  elementsValue(36)
    val LQ1  =  elementsValue(37)
    val WHF  =  elementsValue(38)
    val FK  =  elementsValue(39)
    val CE  =  elementsValue(40)
    val MG  =  elementsValue(41)




    var P01: Double = 0.0
    var P02: Double = 0.0
    var P03: Double = 0.0
    var P04: Double = 0.0
    var P05: Double = 0.0
    var P06: Double = 0.0
    var P07: Double = 0.0
    var P08: Double = 0.0
    var P09: Double = 0.0
    var P10: Double = 0.0
    var P11: Double = 0.0
    var P12: Double = 0.0
    var P13: Double = 0.0
    var P14: Double = 0.0
    var P15: Double = 0.0
    var P16: Double = 0.0
    var P17: Double = 0.0
    var P18: Double = 0.0
    var P19: Double = 0.0
    var P20: Double = 0.0
    var P21: Double = 0.0
    var P22: Double = 0.0
    var P23: Double = 0.0
    var P24: Double = 0.0
    var P25: Double = 0.0
    var P26: Double = 0.0
    var P27: Double = 0.0
    var P28: Double = 0.0
    var P29: Double = 0.0
    var P30: Double = 0.0
    var P31: Double = 0.0
    var P32: Double = 0.0
    var P33: Double = 0.0
    var P34: Double = 0.0
    var P35: Double = 0.0
    var P36: Double = 0.0
    var P37: Double = 0.0
    var P38: Double = 0.0
    var P39: Double = 0.0
    var P40: Double = 0.0
    var P41: Double = 0.0
    var P42: Double = 0.0
    var P43: Double = 0.0
    var P44: Double = 0.0
    var P45: Double = 0.0
    var P46: Double = 0.0
    var P47: Double = 0.0
    var P48: Double = 0.0
    var P49: Double = 0.0
    var FARBE: Double = 0.0


    P01 = CR + MO + NI
    P02 = C + MN
    P03 = MN / S
    P04 = CU + NI + CR + MO
    P05 = NI + MO
    P06 = CU + (10 * SN)
    P04 = CU + NI + CR + MO
    P05 = NI + MO
    P06 = CU + (10 * SN)
    P06 = CU + (10 * SN)
    P07 = P + S
    P08 = CR + MO
    P09 = CR + NI
    P10 = SI + NI + CR + CU
    P11 = V + NB
    P10 = SI + NI + CR + CU
    P11 = V + NB
    P12 = NI + CU
    P13 = CU + (8 * SN)
    P14 = SI + MN + CR
    P15 = CR + MO + CU
    P16 = (3 * C) + (3 * CR) + (2 * MO) + NI
    P17 = CR + NI + CU
    P14 = SI + MN + CR
    P15 = CR + MO + CU
    P16 = (3 * C) + (3 * CR) + (2 * MO) + NI
    P17 = CR + NI + CU
    P18 = AL / N
    P19 = AS + SB + SN
    P20 = NB + TI + V
    P21 = CU + SN
    P22 = SI + (P * 2.5)
    P23 = TI / N
    P24 = CU + (6 * SN)
    P25 = SI + MN + NI + CR + CU + AL
    P26 = C + (2 * MN) + CR + (2 * NI) + (6 * MO)
    P27 = CU + (5 * SN)
    P25 = SI + MN + NI + CR + CU + AL
    P26 = C + (2 * MN) + CR + (2 * NI) + (6 * MO)
    P27 = CU + (5 * SN)
    P28 = (5 * C) + MN
    P29 = CR + MN
    P30 = MO + NI + CU
    P31 = C + (SI / 24) + (MN / 6) + (CR / 5) + (MO / 4) + (NI / 40) +
      (V / 14)
    P32 = MN + (0.35 * NI) + (1.5 * CR) + (2 * MO) + (0.17 * CU)
    P33 = P32 + (C * 3.33)
    P34 = C + (MN / 6) + ((CR + MO + V) / 5) + ((NI + CU) / 15)
    P35 = V + NB + TI + B + ZR + SN
    P36 = (750 * Math.sqrt(C)) + (150 * SI) + (250 * MN) + (150 * CR) +
      (300 * MO) +
      (120 * NI) +
      (200 * CU) +
      (600 * V)
    P37 = SI + P
    P36 = (750 * Math.sqrt(C)) + (150 * SI) + (250 * MN) + (150 * CR) +
      (300 * MO) +
      (120 * NI) +
      (200 * CU) +
      (600 * V)
    P37 = SI + P
    P38 = S / TE
    P39 = 0

    /**  Calculate P39  */
    if (C <= 0.39) {
      P39 = if (MN <= 1.20) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.54 * C) *
        ((3.3333 * MN) + 1) else if (MN <= 1.95) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.54 * C) *
        ((5.10 * MN) - 1.12) else 0
    } else if (C <= 0.55) {
      P39 = if (MN <= 1.20) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.171 + (0.001 * C) + (0.265 * (C * C))) *
        ((3.3333 * MN) + 1) else if (MN <= 1.95) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.171 + (0.001 * C) + (0.265 * (C * C))) *
        ((5.10 * MN) - 1.12) else 0
    } else if (C <= 0.65) {
      P39 = if (MN <= 1.20) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.115 + (0.268 * C) - (0.038 * (C * C))) *
        ((3.3333 * MN) + 1) else if (MN <= 1.95) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.115 + (0.268 * C) - (0.038 * (C * C))) +
        ((5.10 * MN) - 1.12) else 0
    } else if (C <= 0.75) {
      P39 = if (MN <= 1.20) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.143 + (0.2 * C)) +
        ((3.3333 * MN) + 1) else if (MN <= 1.95) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.143 + (0.2 * C)) +
        ((5.10 * MN) - 1.12) else 0
    } else if (C <= 0.90) {
      P39 = if (MN <= 1.20) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.062 + (0.409 * C) - (0.135 * (C * C))) *
        ((3.3333 * MN) + 1) else if (MN <= 1.95) 25.4 *
        ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) *
          (1.00 + (3.00 * MO)) *
          (1.00 + (0.365 * CU)) *
          (1.00 + (1.73 * V))) *
        (0.062 + (0.409 * C) - (0.135 * (C * C))) *
        ((5.10 * MN) - 1.12) else 0
    } else {
    }
    P40 = P39 / 25.4
    P41 = if (N <= 0.0100) if (AL >= 0.020) if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.090) 3 else if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.110) 2 else if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.140) 1 else 0 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      350) 3 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      450) 2 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      600) 1 else 0 else if (AL >= 0.020) if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.080) 3 else if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.100) 2 else if (AL <=
      -0.0002 *
        ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) +
        0.140) 1 else 0 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      300) 3 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      400) 2 else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <=
      600) 1 else 0
    if (C <= 0.39) {
      P49 = if (MN <= 1.20) 1 else if (MN <= 1.95) 2 else 0
    } else if (C <= 0.55) {
      P49 = if (MN <= 1.20) 3 else if (MN <= 1.95) 4 else 0
    } else if (C <= 0.65) {
      P49 = if (MN <= 1.20) 5 else if (MN <= 1.95) 6 else 0
    } else if (C <= 0.75) {
      P49 = if (MN <= 1.20) 7 else if (MN <= 1.95) 8 else 0
    } else if (C <= 0.90) {
      P49 = if (MN <= 1.20) 9 else if (MN <= 1.95) 10 else 0
    } else {
    }
    FARBE = P41

    P43 = C+SI/28+(MN+MO)/3.7+CR/6.45

    P44 = TI-(5*N-0.003)

    P45 = 10*SB/AS

    P46 = (MN+SI)*(P+SN)*10000

    P47 = CR+MO+NI+CU+V

    P48 = P+SN

    println("P01 = " + P01 )
    println("P02 = " + P02 )
    println("P03 = " + P03 )
    println("P04 = " + P04 )
    println("P05 = " + P05 )
    println("P06 = " + P06 )
    println("P07 = " + P07 )
    println("P08 = " + P08 )
    println("P09 = " + P09 )
    println("P10 = " + P10 )
    println("P11 = " + P11 )
    println("P12 = " + P12 )
    println("P13 = " + P13 )
    println("P14 = " + P14 )
    println("P15 = " + P15 )
    println("P16 = " + P16 )
    println("P17 = " + P17 )
    println("P18 = " + P18 )
    println("P19 = " + P19 )
    println("P20 = " + P20 )
    println("P21 = " + P21 )
    println("P22 = " + P22 )
    println("P23 = " + P23 )
    println("P24 = " + P24 )
    println("P25 = " + P25 )
    println("P26 = " + P26 )
    println("P27 = " + P27 )
    println("P28 = " + P28 )
    println("P29 = " + P29 )
    println("P30 = " + P30 )
    println("P31 = " + P31 )
    println("P32 = " + P32 )
    println("P33 = " + P33 )
    println("P34 = " + P34 )
    println("P35 = " + P35 )
    println("P36 = " + P36 )
    println("P37 = " + P37 )
    println("P38 = " + P38 )
    println("P39 = " + P39 )
    println("P40 = " + P40 )
    println("P41 = " + P41 )
    println("P42 = " + P42 )
    println("P43 = " + P43 )
    println("P44 = " + P44 )
    println("P45 = " + P45 )
    println("P46 = " + P46 )
    println("P47 = " + P47 )
    println("P48 = " + P48 )
    println("P49 = " + P49 )
    println("FARBE = " + FARBE)

    val splitValue = constantFields.split(",")
    val createdTime = splitValue(0)
    val measuredTime = splitValue(1)
    val analysisType = splitValue(2)
    val analysisTask = splitValue(3)
    val analysisMethod = splitValue(4)
    val sID = splitValue(5)


    val query  = "INSERT INTO computation " +
      "(CREATED_TIME,"+
      "MEASURED_TIME," +
      "ANALYSIS_TYPE," +
      "ANALYSIS_TASK," +
      "ANALYSIS_METHOD," +
      "SID," +
      "P01," +
      "P02," +
      "P03," +
      "P04," +
      "P05," +
      "P06," +
      "P07," +
      "P08," +
      "P09," +
      "P10," +
      "P11," +
      "P12," +
      "P13," +
      "P14," +
      "P15," +
      "P16," +
      "P17," +
      "P18," +
      "P19," +
      "P20," +
      "P21," +
      "P22," +
      "P23," +
      "P24," +
      "P25," +
      "P26," +
      "P27," +
      "P28," +
      "P29," +
      "P30," +
      "P31," +
      "P32," +
      "P33," +
      "P34," +
      "P35," +
      "P36," +
      "P37," +
      "P38," +
      "P39," +
      "P40," +
      "P41," +
      "P42," +
      "P43," +
      "P44," +
      "P45," +
      "P46," +
      "P47," +
      "P48," +
      "P49) " +
      "VALUES ('" +
      createdTime +"','"+
      measuredTime +"','"+
      analysisType +"','"+
      analysisTask +"','"+
      analysisMethod +"','"+
      sID +"','"+
      P01 +"','"+
      P02+ "','" +
      P03 + "','" +
      P04 + "','" +
      P05 + "','" +
      P06 + "','" +
      P07 +  "','" +
      P08 + "','" +
      P09 + "','" +
      P10 +"','"+
      P11 +"','"+
      P12 +"','"+
      P13 +"','"+
      P14 +"','"+
      P15 +"','"+
      P16 +"','"+
      P17 +"','"+
      P18 +"','"+
      P19 +"','"+
      P20 +"','"+
      P21 +"','"+
      P22 +"','"+
      P23 +"','"+
      P24 +"','"+
      P25 +"','"+
      P26 +"','"+
      P27 +"','"+
      P28 +"','"+
      P29 +"','"+
      P30 +"','"+
      P31 +"','"+
      P32 +"','"+
      P33 +"','"+
      P34 +"','"+
      P35 +"','"+
      P36 +"','"+
      P37 +"','"+
      P38 +"','"+
      P39 +"','"+
      P40 +"','"+
      P41 +"','"+
      P42 +"','"+
      P43 +"','"+
      P44 +"','"+
      P45 +"','"+
      P46 +"','"+
      P47 +"','"+
      P48 +"','"+ P49 +"')"

    new ExecuteSQLQuery(query,"localhost",3306,"TEST","root","Password!@").executeQuery()
  }
}
