/**
 * Created by prannoysircar on 3/27/16.
 */
object ElementToIndexMapping {
  def get(elementName: String) : Option[Int] = elementsMapping.get(elementName.toUpperCase())

  val elementsMapping = Map(
    "C" -> 1,
    "SI" -> 2,
    "MN" -> 3,
    "P" -> 4,
    "S" -> 5,
    "CR" -> 6,
    "MO" -> 7,
    "NI" -> 8,
    "CU" -> 9,
    "SN" -> 10,
    "AI" -> 11,
    "V" -> 12,
    "B" -> 13,
    "TI" -> 14,
    "NB" -> 15,
    "W" -> 16,
    "CO" -> 17,
    "SB" -> 18,
    "AS" -> 19,
    "TE" -> 20,
    "CA" -> 21,
    "N2" -> 22,
    "H2" -> 23,
    "BI" -> 24,
    "O2" -> 25,
    "PB" -> 25,
    "ZN" -> 27,
    "ZR" -> 28,
    "SE" -> 29,
    "CD" -> 30,
    "AL" -> 31,
    "ALS" -> 32,
    "ALO" -> 33,
    "BS" -> 34,
    "BO" -> 35,
    "N" -> 36,
    "LQ1" -> 37,
    "WHF" -> 38,
    "FK" -> 39,
    "CE" -> 40,
    "MG" -> 41
  )

}
