import com.stratio.receiver.RabbitMQUtils
import com.typesafe.config.ConfigFactory
import org.apache.spark.streaming.{StreamingContext, Seconds}
import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by prannoysircar on 3/8/16.
 */
object RabbitMQ {

  def main(args: scala.Array[String]): Unit = {

    val config = ConfigFactory.load("myFile.conf")

    val conf = new SparkConf()
      .setAppName("RabbitMQ")
      .setMaster(config.getString("master"))

    val sc = new SparkContext(conf)
    sc.addJar(config.getString("project.jar.path"))

    val ssc = new StreamingContext(sc, Seconds(5))

    val receiverStream = RabbitMQUtils.createStream(ssc,Map(
      "host" -> config.getString("queue.hostname"),
      "queueName" -> config.getString("queue.queuename"),
      "username" -> config.getString("queue.username"),
      "password" -> config.getString("queue.password"))
    )

    receiverStream.foreachRDD(msg =>
    {


      msg.foreach(x => {

        val  data = scala.xml.XML.loadString(x)
        val action = data.nonEmptyChildren(1).label
        val sid = (data \\ action \ "@sid" ) text
        val time = (data \\ "Interface" \ "@time" ) text
        val posno = (data \\ action \ "@posno" ) text
        val typeValue = (data \\ action \ "@type" ) text
        val instrument = "DUMMY"

        println("action " + action)
        println("sid " + sid)
        println("time " + time)
        println("posno " + posno)
        println("typeValue " + typeValue)


      })

    }
    )

    receiverStream.print()

    ssc.start()
    ssc.awaitTermination()


  }

}
