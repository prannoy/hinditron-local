import java.io.File

import org.apache.spark.SparkConf
import org.apache.spark._
import org.elasticsearch.spark.rdd.EsSpark
import com.typesafe.config.ConfigFactory

/**
 * Created by prannoysircar on 3/4/16.
 */


object OES {

  def main(args: scala.Array[String]): Unit = {

    println("Starting OES Pipeline")


    val config = ConfigFactory.load("myFile.conf")

    val conf = new SparkConf()
      .setAppName("CsvStream")
      .set("es.nodes", config.getString("es.nodes"))
      .setMaster(config.getString("master"))

    val sc = new SparkContext(conf)
    sc.addJar(config.getString("project.jar.path"))

    val batch = sc.textFile(config.getString("oes.data.source"))

    val pairResult = batch.flatMap(line => {
      val splitValue = line.split(",")

      val createdTime = Seq(splitValue(ColumnNameConvention.DATE_17.id) , splitValue(ColumnNameConvention.TIME_18.id)).mkString(" ")
      val analysisType = splitValue(ColumnNameConvention.NORMAL_ANALYSIS_0.id)
      val analysisTask = splitValue(ColumnNameConvention.ANALYSIS_TASK_6.id)
      val analysisMethod = splitValue(ColumnNameConvention.ANALYSIS_METHOD_7.id)
      val sID = splitValue(ColumnNameConvention.SAMPLE_ID_CHANNEL_1_14.id) + "-" + splitValue(ColumnNameConvention.SAMPLE_ID_CHANNEL_2_15.id) + "-" + splitValue(ColumnNameConvention.SAMPLE_POINT_16.id) + "_" + splitValue(ColumnNameConvention.DATE_17.id) + " " + splitValue(ColumnNameConvention.TIME_18.id)
      val numberOfElements = splitValue(ColumnNameConvention.TOTAL_NUMBER_OF_ELEMENTS_24.id).toInt
      val source = "To be Defined"
      val date = "20" + splitValue(ColumnNameConvention.MEASURED_DATE_AND_TIME_YEAR_5.id) + "-" + splitValue(ColumnNameConvention.MEASURED_DATE_AND_TIME_MONTH_4.id) + "-" + splitValue(ColumnNameConvention.MEASURED_DATE_AND_TIME_DAY_3.id)
      val hourMinute = splitValue(ColumnNameConvention.MEASURED_DATE_AND_TIME_HOUR_1.id) + ":" + splitValue(ColumnNameConvention.MEASURED_DATE_AND_TIME_MINUTE_2.id)
      val measuredTime = Seq(date,hourMinute).mkString(" ")

      val initPos = 25
      val finalPos = splitValue.length - 1
      val step = 3

      //      assert(numberOfElements * 3 == finalPos - initPos + 1)

      val constantFields = Seq(createdTime, measuredTime, analysisType, analysisTask, analysisMethod, sID, numberOfElements).mkString(",")
      /**
       * Array, where each index correspond to a element.
       **/
      val elementArray = Array.ofDim[Double](100)

      val tempList = Range.apply(initPos, finalPos, step).map { index =>

        /**
         * Set element name
         **/
        val elementName = splitValue(index)


        /**
         * Set element value
         *
         * Index of element value is always plus 2
         * i.e. if index of element name is "a" index of element value will be a+2
         *
         **/
        val elementValue = splitValue(index + 2).toDouble

        val elementIndex = ElementToIndexMapping.get(elementName).get
        elementArray(elementIndex) = elementValue

        /**
         * Set value of flag based on the flag sign
         *
         * Index of flag is one value ahead of index of element name i.e. a+1
         **/

        val flagValue =
          if (FlagSymbolToValueMapping.exists(_._1 == splitValue(index + 1))) {
            val flagSign = splitValue(index + 1)
            FlagSymbolToValueMapping.get(flagSign).get
          }
          else {
            "NOT PRESENT"
          }

        val query = "INSERT INTO TEST_TABLE_CSV2 " +
          "(CREATED_TIME," +
          "MEASURED_TIME," +
          "ANALYSIS_TYPE," +
          "ANALYSIS_TASK," +
          "ANALYSIS_METHOD," +
          "SID," +
          "NUMBER_OF_ELEMENTS," +
          "ELEMENT_NAME," +
          "ELEMENT_VALUE," +
          "ELEMENT_FLAG," +
          "SOURCE," +
          "MEASURED_DATE) " +
          "VALUES ('" + createdTime + "','" + measuredTime + "','" + analysisType + "','" + analysisTask + "','" + analysisMethod + "','" + sID + "','" + numberOfElements + "','" + elementName + "','" + elementValue + "','" + flagValue + "','" + source + "','" + date + "')"

        new ExecuteSQLQuery(query, "localhost", 3306, "TEST", "root", "Password!@").executeQuery()

        Seq(constantFields, elementName, elementValue, flagValue).mkString(",")
      }

      val obj: Compute = new Compute(constantFields, elementArray)
      obj.compute()

      tempList
    })


    val esMappedRDD = pairResult.map(line => {
      val splitValue = line.split(",")

      val createdTime = splitValue(0)
      val measuredTime = splitValue(1)
      val analysisType = splitValue(2)
      val analysisTask = splitValue(3)
      val analysisMethod = splitValue(4)
      val sID = splitValue(5)
      val numberOfElements = splitValue(6)
      val elementName = splitValue(7)
      val elementValue = splitValue(8)
      val flagValue = splitValue(9)
      val source = "OES"
      //val date = splitValue(10)

      Map(
        "CREATED_TIME" -> createdTime,
        "MEASURED_TIME" -> measuredTime,
        "ANALYSIS_TYPE" -> analysisType,
        "ANALYSIS_TASK" -> analysisTask,
        "ANALYSIS_METHOD" -> analysisMethod,
        "SID" -> sID,
        "NUMBER_OF_ELEMENTS" -> numberOfElements,
        "ELEMENT_NAME" -> elementName,
        "ELEMENT_VALUE" -> elementValue,
        "ELEMENT_FLAG" -> flagValue,
        "SOURCE" -> source
      )
    })

    EsSpark.saveToEs(esMappedRDD, "testing4/test")

    sc.stop()
  }
}