import java.sql.{SQLException, DriverManager, Statement, Connection}

/**
 * Created by prannoysircar on 3/27/16.
 */
class ExecuteSQLQuery(QUERY :String, HOST :String, PORT :Int, DBNAME :String, USER :String, PASSWORD :String) {

  def executeQuery(): Unit = {

    val JDBC_DRIVER = "com.mysql.jdbc.Driver"
    val DB_URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DBNAME

    var conn: Connection = null;

    var stmt: Statement = null;

    Class.forName("com.mysql.jdbc.Driver")
    conn = DriverManager.getConnection(DB_URL, USER, PASSWORD)
    stmt = conn.createStatement()

    try {
      println(QUERY)
      stmt.executeUpdate(QUERY)
    } catch {
      case se: SQLException => se.printStackTrace
    }

    // close DB connection
    try {
      if (stmt != null)
        conn.close()
    } catch {
      case se: SQLException => ;
    }
    try {
      if (conn != null)
        conn.close()
    } catch {
      case se: SQLException => se.printStackTrace()
    }

  }

}
