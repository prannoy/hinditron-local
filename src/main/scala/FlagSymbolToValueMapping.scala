/**
 * Created by prannoysircar on 3/27/16.
 */
object FlagSymbolToValueMapping {
  def get(symbol: String): Option[String] = symbolToValueMap.get(symbol)

  def exists(p: ((String, String)) => Boolean): Boolean = symbolToValueMap.exists(p)

  val symbolToValueMap = Map(
    ("!" -> "Below the Limit of Detection (LOD or DL)"),
    (">" -> "Above specification - positive values only"),
    ("<" -> "Below specification - positive values only"),
    (")" -> "Above the Calibration Range or the Validity Range of the Internal Standard"),
    ("(" -> "Below the Calibration Range or the Validity Range of the Internal Standard"),
    ("]" -> "Above the Upper Control Limit (UCL)"),
    ("[" -> "Below the Lower Control Limit (LCL)"),
    ("*" -> "Element not reproducible when Reproducibility checking is being used"),
    ("}" -> "Above the Pseudo-element validity range"),
    ("{" -> "Below the Pseudo-element validity range"),
    ("^" -> "Channel overflow"),
    ("v" -> "Channel underflow"),
    ("#" -> "Failed SPC test")
  )

}
