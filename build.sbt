name := "hinditron_local"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" % "spark-streaming_2.10" % "1.6.0"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.38"

libraryDependencies += "org.apache.spark" % "spark-sql_2.10" % "1.6.0"

libraryDependencies += "com.stratio.receiver" % "spark-rabbitmq_1.4" % "0.2.0"

libraryDependencies += "com.typesafe.play" % "play-json_2.10" % "2.4.6"

libraryDependencies += "com.typesafe" % "config" % "1.3.0"

libraryDependencies += "com.sksamuel.elastic4s" % "elastic4s-core_2.10" % "2.2.1"
//libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "2.2.0"
dependencyOverrides ++= Set(
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.4.4"
)